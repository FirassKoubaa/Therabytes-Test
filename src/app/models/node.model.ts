/**
 * Created by firas on 30/09/17.
 */
export class Node {
  public name: string;
  public isLocked: boolean;
  public canBeUnlocked: boolean;
  public parentNodes: Node[];
  public childNodes: Node[];


  constructor(name: string,
              isLocked: boolean,
              canBeUnlocked: boolean,
              parentNodes: Node[],
              childNodes: Node[]) {
    this.name = name;
    this.isLocked = isLocked;
    this.canBeUnlocked = canBeUnlocked;
    this.parentNodes = parentNodes;
    this.childNodes = childNodes;
  }

}
