import { TestBed, inject } from '@angular/core/testing';

import { LockingNodeService } from './locking-node.service';

describe('LockingNodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LockingNodeService]
    });
  });

  it('should be created', inject([LockingNodeService], (service: LockingNodeService) => {
    expect(service).toBeTruthy();
  }));
});
