import { Injectable } from '@angular/core';

@Injectable()
export class LockingNodeService {

  clickedNodeChilds: any;
  constructor() { }

  public unlockNodes(node , selectedNode , nodesList) {
    selectedNode = node.name;
    // Search the selected element and unlock it if it canBeUnlocked
    const clickedNodeObj = nodesList.find(( n => n.name === selectedNode));
    if (clickedNodeObj.canBeUnlocked === true) {
      clickedNodeObj.isLocked = false;
    }
    // when node clicked every child canBeUnlocked
    this.clickedNodeChilds = clickedNodeObj.childNodes;
    for (const i of this.clickedNodeChilds) {
      if (clickedNodeObj.isLocked === false) {
        i.canBeUnlocked = true;
      }
    }
    // Test if there is a child whom one of parents is locked
    this.testOfLockedParent(nodesList);
  }

  public testOfLockedParent(nodesList) {
    for (const i of nodesList) {
      if (i.isLocked === true) {
        for (const j of i.childNodes) {
          j.canBeUnlocked = false;
        }
      }
    }
  }



}
