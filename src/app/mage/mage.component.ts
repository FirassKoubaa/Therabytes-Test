import {Component, OnInit} from '@angular/core';
import {Node} from '../models/node.model';
import {LockingNodeService} from '../services/locking-node.service';

@Component({
  selector: 'app-mage',
  templateUrl: './mage.component.html',
  styleUrls: ['./mage.component.css'],
  providers: [LockingNodeService]

})
export class MageComponent implements OnInit {
  // Node names
  mageNodesList= [];
  Mage: any;
  Fireball: any;
  ElectronShock: any;
  Freeze: any;
  Thunderbolt: any;
  Snowstorm: any;
  // Initial -Mage- values
  nodeName = 'Mage';
  nodeLocked = false;
  nodeCanBeUnlocked = true;
  // On Select values
  selectedNode: any;

  constructor(private lockingNodeService: LockingNodeService) {
    this.Mage = new Node('Mage', true, true , [], []);
    this.Fireball = new Node('Fireball', true, false, [], []);
    this.ElectronShock = new Node('ElectronShock', true, false , [], []);
    this.Freeze = new Node('Freeze', true, false , [], []);
    this.Thunderbolt = new Node('Thunderbolt', true, false , [], []);
    this.Snowstorm = new Node('Snowstorm', true, false , [], []);
    }

  ngOnInit() {
    this.Mage.childNodes = [this.Fireball];
    this.Fireball.parentNodes = [this.Mage];
    this.Fireball.childNodes = [this.ElectronShock , this.Freeze];
    this.ElectronShock.parentNodes = [this.Fireball];
    this.ElectronShock.childNodes = [this.Thunderbolt];
    this.Freeze.parentNodes = [this.Fireball];
    this.Freeze.childNodes = [this.Snowstorm];
    this.Thunderbolt.parentNodes = [this.ElectronShock];
    this.Snowstorm.parentNodes = [this.Freeze];
    // The Mage Graph object list
    this.mageNodesList = [this.Mage, this.Fireball, this.ElectronShock , this.Freeze , this.Thunderbolt , this.Snowstorm];
  }
  public display(event, node) {
    this.nodeName = node.name;
    this.nodeLocked = node.isLocked;
    this.nodeCanBeUnlocked = node.canBeUnlocked;
  }

  public unlock(event, node) {
    this.lockingNodeService.unlockNodes(node , this.selectedNode , this.mageNodesList );
  }
}
