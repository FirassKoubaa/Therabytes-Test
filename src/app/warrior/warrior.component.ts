import {Component, OnInit} from '@angular/core';
import {Node} from '../models/node.model';
import {LockingNodeService} from '../services/locking-node.service';

@Component({
  selector: 'app-warrior',
  templateUrl: './warrior.component.html',
  styleUrls: ['./warrior.component.css'],
  providers: [LockingNodeService]

})
export class WarriorComponent implements OnInit {
  // Node names
  warriorNodesList = [];
  Warrior: any;
  Strike: any;
  Hit: any;
  DoubleStrike: any;
  Slash: any;
  Knockout: any;
  RoundhouseKick: any;
  // Initial -Warrior- values
  nodeName = 'Warrior';
  nodeLocked = false;
  nodeCanBeUnlocked = true;
  // On Select values
  selectedNode: any;

  constructor(private lockingNodeService: LockingNodeService) {
    this.Warrior = new Node('Warrior', true, true, [], []);
    this.Strike = new Node('Strike', true, false, [], []);
    this.Hit = new Node('Hit', true, false, [], []);
    this.DoubleStrike = new Node('DoubleStrike', true, false, [], []);
    this.Slash = new Node('Slash', true, false, [], []);
    this.Knockout = new Node('Knockout', true, false, [], []);
    this.RoundhouseKick = new Node('RoundhouseKick', true, false, [], []);
  }

  ngOnInit() {
    this.Warrior.childNodes = [this.Strike, this.Hit];
    this.Strike.parentNodes = [this.Warrior];
    this.Strike.childNodes = [this.DoubleStrike, this.Slash];
    this.Hit.parentNodes = [this.Warrior];
    this.Hit.childNodes = [this.Knockout];
    this.DoubleStrike.parentNodes = [this.Strike];
    this.Slash.parentNodes = [this.Strike];
    this.Slash.childNodes = [this.RoundhouseKick];
    this.Knockout.parentNodes = [this.Hit];
    this.Knockout.childNodes = [this.RoundhouseKick];
    // The Warrior Graph object list
    this.warriorNodesList = [this.Warrior, this.Strike, this.Hit, this.DoubleStrike, this.Slash, this.Knockout, this.RoundhouseKick];
  }
  public display(event, node) {
    this.nodeName = node.name;
    this.nodeLocked = node.isLocked;
    this.nodeCanBeUnlocked = node.canBeUnlocked;
  }

  public unlock(event, node) {
    this.lockingNodeService.unlockNodes(node , this.selectedNode , this.warriorNodesList );
  }
}
